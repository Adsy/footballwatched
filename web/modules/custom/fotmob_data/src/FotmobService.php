<?php

namespace Drupal\fotmob_data;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class CustomService
 * @package Drupal\fotmob_data\Services
 */
class FotmobService
{

    protected AccountInterface $currentUser;

    /**
     * The HTTP client to fetch the feed data with.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * CustomService constructor.
     * @param AccountInterface $currentUser
     * @param ClientInterface $http_client
     */
    public function __construct(AccountInterface $currentUser, ClientInterface $http_client)
    {
        $this->currentUser = $currentUser;
        $this->httpClient = $http_client;
    }


    /**
     * @return MarkupInterface|string
     */
    public function getData()
    {
        return $this->currentUser->getDisplayName();
    }


    public function getMatchesByDate(string $date)
    {
        $prefixUrl = sprintf('api/matches?date=%s', $date);
        return $this->fotmobCall('GET', $prefixUrl);
    }

    /**
     *  Appel API fotmob.
     * @param string $method
     * @param string $prefixUrl
     * @param array $data
     * @return array
     */
    private function fotmobCall(string $method, string $prefixUrl, array $data = []) {
        $url = sprintf('https://www.fotmob.com/%s', $prefixUrl);

        $result = $this->httpClient->request(
            $method,
            $url,
            $data,
        );

        if ($result->getStatusCode() === 200) {
            $bodyEncoded = $result->getBody()->getContents();
            return json_decode($bodyEncoded, TRUE);
        }

        return NULL;
    }
}
