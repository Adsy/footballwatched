<?php

namespace Drupal\fotmob_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\fotmob_data\FotmobService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for fotmob_data routes.
 */
class FotmobController extends ControllerBase
{

    /**
     * The fotmob_data service.
     *
     * @var FotmobService
     */
    protected $fotmobService;

    /**
     * @param FotmobService $fotmobService
     */
    public function __construct(FotmobService $fotmobService)
    {
        $this->fotmobService = $fotmobService;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('fotmob_data.service')
        );
    }

    /**
     * Builds the response.
     */
    public function build()
    {
        // Récupération de la date.
        $date = \Drupal::request()->get('date');
        $date = $date ?? date('Ymd');

        // Récupération des matchs du jour.
        $data = $this->fotmobService->getMatchesByDate($date);

        $leagues = $data['leagues'];

        // Champ date exposée.
        $build['#date'] = [
            '#type' => 'date',
            '#name' => 'date',
            '#value' => date('Y-m-d', strtotime($date)),
        ];

        $build['#search'] = [
            '#type' => 'search',
            '#attributes' => [
                'placeholder' => 'Pays, championnat, équipe'
            ],
        ];

        $build['#theme'] = 'fotmob_home';
        $build['#leagues'] = $leagues;

        $build['#attached']['library'][] = 'fotmob_data/fotmob_data';

        // Construction select watched.
        $options = [
            'pasvu' => 'Non regardé',
            'entier' => 'Regardé en entier',
            'partiellement' => 'Regardé à moitié / en coup d\'oeil',
            'resume' => 'Regardé le résumé',
            'stade' => 'Vu au stade',
        ];

        $build['#select_watched'] = [
            '#type' => 'select',
            '#options' => $options
        ];

        return $build;
    }

}
