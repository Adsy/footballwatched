(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.fotmob = {
        attach:function (context) {
            $('input[name="date"]').change(function () {
                const date = $(this).val().replaceAll('-', '')
                window.location.href = window.location.origin + '?date=' + date
            });
        }
    };

})(jQuery, Drupal, drupalSettings);